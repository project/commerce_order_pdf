CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Commerce Order PDF provides a PDF receipt functionality for all the orders. It
uses DOM PDF library to generate PDF.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_order_pdf

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_order_pdf


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Drupal Commerce - https://www.drupal.org/project/commerce
 * DOMPDF library


INSTALLATION
------------

 * Install the Commerce Order PDF module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * As this module is having dependencies of DOMPDF library and commerce modules
   We prefer that you install this module using COMPOSER like run this
   command (composer require 'drupal/commerce_order_pdf:1.x-dev') in the
   Drupal site root folder.


CONFIGURATION
-------------

 * Navigate to Administration > Extend and enable the module.
 * Navigate to Administration > Commerce > Commerce Order PDF Setting to configure.

 * Edit the HTML and CSS as desired.

 * Save configuration.


USAGE
-----

 * After installation and configuration of the module, navigate to :-
   Administration > Commerce > Orders

 * You can create new orders if no orders are listed already.

 * After the order has been created, the PDF reciept can be viewed on the page :-
   '/commerce/order/{commerce_order_id}/pdf'.

 * Administrator and the user with access can see the invoice.


MAINTAINERS
-----------

 * Gaurav Chauhan (gchauhan) - https://www.drupal.org/u/gchauhan
 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
